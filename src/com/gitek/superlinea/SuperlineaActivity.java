package com.gitek.superlinea;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

@SuppressLint("SetJavaScriptEnabled")
public class SuperlineaActivity extends Activity {

	private Button btnPreferencias;
	private Button btnRecargar;
	private TextView myTitle;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_superlinea);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        
        SharedPreferences pref =
				PreferenceManager.getDefaultSharedPreferences(
						SuperlineaActivity.this);
			
		
        Toast toast1 =
         			Toast.makeText(getApplicationContext(),
        					pref.getString("opcCodigo", ""), Toast.LENGTH_LONG);
		     
        toast1.show();

        String url=pref.getString("opcUrl", "");
        String tablet = pref.getString("opcCodigo", "");
        String miUrl =url+tablet;

        Toast toast2 =
    			Toast.makeText(getApplicationContext(),
    					miUrl, Toast.LENGTH_LONG);
	     
        toast2.show();
    
        WebView engine = (WebView) findViewById(R.id.web_engine);
        engine.loadUrl(miUrl);
        engine.getSettings().setJavaScriptEnabled(true);
        
        
        btnPreferencias = (Button)findViewById(R.id.BtnPreferencias);
        btnRecargar = (Button)findViewById(R.id.recargar);
        myTitle = (TextView)findViewById(R.id.myTitle);
        
        myTitle.setText(tablet);
        
        btnPreferencias.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(SuperlineaActivity.this, 
						                 Opciones.class));

			}
		});
        
        btnRecargar.setOnClickListener(new View.OnClickListener() {
        	public void onClick(View v) {
        		SharedPreferences pref =
        				PreferenceManager.getDefaultSharedPreferences(
        						SuperlineaActivity.this);
        		String url=pref.getString("opcUrl", "");
                String tablet = pref.getString("opcCodigo", "");
                String miUrl =url+tablet;
                Toast toast2 =
            			Toast.makeText(getApplicationContext(),
            					miUrl, Toast.LENGTH_LONG);
        	     
                toast2.show();
                WebView engine = (WebView) findViewById(R.id.web_engine);
                engine.loadUrl(miUrl);
                engine.getSettings().setJavaScriptEnabled(true);
        	}
        });
        
    }
    @Override
    public void onResume()
    {
        super.onResume();
        SharedPreferences pref =
				PreferenceManager.getDefaultSharedPreferences(
						SuperlineaActivity.this);
        String tablet = pref.getString("opcCodigo", "");
        TextView myTitle;
        myTitle = (TextView)findViewById(R.id.myTitle);
        myTitle.setText(tablet);
    }
     
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_superlinea, menu);
        return true;
    }
}
