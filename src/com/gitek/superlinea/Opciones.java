package com.gitek.superlinea;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.util.Log;

public class Opciones extends PreferenceActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addPreferencesFromResource(R.xml.opciones);
   
        SharedPreferences sp = getPreferenceScreen().getSharedPreferences();
        
        final EditTextPreference editTextPref = (EditTextPreference) getPreferenceManager().findPreference("opcCodigo");
        editTextPref.setSummary(sp.getString("opcCodigo", ""));
        
        editTextPref.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Log.d("MyApp", "Pref " + preference.getKey() + " changed to " + newValue.toString());
                EditTextPreference etp = (EditTextPreference) preference;
                String newHostValue = newValue.toString();
                etp.setSummary(newHostValue);                
                return true;
            }
        });
        
        final EditTextPreference editTextPref2 = (EditTextPreference) getPreferenceManager().findPreference("opcUrl");
        editTextPref2.setSummary(sp.getString("opcUrl", ""));
        
        editTextPref2.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {            
            public boolean onPreferenceChange(Preference preference, Object newValue) {
                Log.d("MyApp", "Pref " + preference.getKey() + " changed to " + newValue.toString());
                EditTextPreference etp = (EditTextPreference) preference;
                String newHostValue = newValue.toString();
                etp.setSummary(newHostValue);
                return true;
            }
        });
         
    }
    
    public boolean onPreferenceChange(Preference preference, Object newValue) {
    	EditTextPreference etp = (EditTextPreference) preference;
        String newHostValue = newValue.toString();
        etp.setTitle(newHostValue);
        return true;
    }
    
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        Preference pref = findPreference(key);
        if (pref instanceof EditTextPreference) {
            EditTextPreference etp = (EditTextPreference) pref;
            pref.setSummary(etp.getText());
        }
    }
}
